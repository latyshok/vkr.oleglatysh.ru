<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $names[1] = ['Алексей', 'Александр', 'Олег', 'Виталий', 'Василий', 'Николай', 'Петр'];
       $sur[1] = ['Петрович', 'Михайлович', 'Константинович', 'Федорович', 'Васильевич', 'Генадьевич'];
       $last[1] = ['Васильев', 'Белов', 'Латыш', 'Иванов', 'Акимов', 'Бадражан', 'Зиновьев'];

        $names[2] = ['Дарья', 'Анастасия', 'Анна', 'Виктория', 'Диана', 'Ирина', 'Любовь'];
        $sur[2] = ['Петровна', 'Михайловна', 'Константиновна', 'Федоровна', 'Васильевна', 'Генадьевна'];
        $last[2] = ['Чунаева', 'Шушина', 'Малова', 'Николаева', 'Курекина', 'Розина', 'Валеева'];

       for ($i = 0;$i <= 40; $i++){

           $range = rand (1, 2);

           DB::table('users')->insert([
               'name' => $last[$range][rand(0, count($last[$range]) - 1)].' '.$names[$range][rand(0, count($names[$range]) - 1)].' '.$sur[$range][rand(0, count($sur[$range]) - 1)],
               'email' => str_random(10).'@example.com',
               'password' => bcrypt('secret'),
               'role' => 'student'
           ]);
       }

        DB::table('users')->insert([
            'name' => 'Латыш Олег Константинович',
            'email' => 'olegjobmail@mail.ru',
            'password' => bcrypt('qwerty12'),
            'role' => 'admin'
        ]);
    }
}
