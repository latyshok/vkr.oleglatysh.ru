<?php
/**
 * Created by PhpStorm.
 * User: black
 * Date: 06.01.2019
 * Time: 18:13
 */

return [
    'Welcome' => 'Добро пожаловать в личный кабинет',
    'Login' => 'Вход',
    'Register' => 'Регистрация',
    'Logout' => 'Выйти',
    'E-Mail Address' => 'E-Mail Адрес',
    'Password' => 'Пароль',
    'Remember Me' => 'Запомнить меня',
    'Forgot Your Password?' => 'Забыли пароль ?',
    'Reset Password' => 'Восстановление пароля',
    'Send Password Reset Link' => 'Получить ссылку для сброса пароля',
    'Confirm Password' => 'Восстановить пароль',
];