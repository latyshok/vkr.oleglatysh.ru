@foreach($groups as $group)
<tr data-discipline_class="{{ $group['class_room'] }}">
    <td>{{ $group['name'] }}</td>
    <td><button onclick="delDisciplineGroup('{{ $group['class_room'] }}', '{{ $discipline_id }}')" type="button" class="btn btn-outline-danger btn-sm">✖</button></td>
</tr>
@endforeach
<tr>
    @if(count($list_groups))
    <td>
        <div class="form-group">
            <select class="form-control form-control-sm" id="selectGroup">
                @foreach($list_groups as $group)
                <option value="{{ $group->id }}">{{ $group->name }}</option>
                @endforeach
            </select>
        </div>
    </td>
    <td>
        <button onclick="$(this).prop('disabled', true); addDisciplineGroup('{{ $user['id'] }}', '{{ $discipline_id }}')" type="button" class="btn btn-primary btn-sm">Добавить</button>
    </td>
    @endif
</tr>