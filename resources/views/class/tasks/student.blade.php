<div class="col-lg-10">

    <div class="card">
        <div class="card-header">{{ $discipline->name }}</div>

        <div class="card-body" id="tasks">

            <div class="row">

                @foreach($tasks as $task)
                    <div data-task_id="{{ $task['task']->id }}" class="col-sm-12 col-lg-6 task">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">{{ $task['task']->title }}</h5>
                                <p class="card-text">
                                    @if(strlen($task['task']->text) < 150)
                                        {{ $task['task']->text }}
                                    @else
                                        @short( $task['task']->text )
                                    @endif
                                </p>
                                    <button onclick="showTask('{{ $task['task']->id }}')" class="btn btn-primary btn-sm">Просмотр</button>
                            </div>
                            @if( isset($task['answer']) && $task['answer']->mark )
                                <div class="alert alert-success text-right" role="alert">
                                    Ваша оценка: {{ $task['answer']->mark }}/5
                                </div>
                            @elseif($task['answer'])
                                <div class="alert alert-info text-right">
                                    Задание на проверке
                                </div>
                            @else
                                <div class="alert alert-danger text-right">
                                    Задание не выполнено
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

    </div>

</div>

<div class="modal fade view" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>

<script>

    function showTask(id){
        axios.post("{{ route('show_task') }}", {
            id : id,
        })
            .then(function (r) {
                $('.view .modal-content').html(r.data);
                $('.view').modal()
            })
            .catch(function (e) {
                console.log('DelTask: ',e);
            });
    }

    document.addEventListener("DOMContentLoaded", function () {
        // на доработке
        // $('#text').summernote();
    });
</script>