@extends('layouts.app')
@section('header')
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/jquery-confirm.min.js') }}" defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="{{ asset('lib/js/summernote.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('lib/css/summernote.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/jquery-confirm.min.css') }}" rel="stylesheet">
    </head>
@show
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @switch($user['role'])
                @case('teacher')
                    @include('class.tasks.teacher')
                @break
                @case('student')
                    @include('class.tasks.student')
                @break
                @default
                    <p>Вы не являетесь преподавателем или студентом, функционал данной страницы вам недоступен</p>
                @break
            @endswitch
        </div>
    </div>
@endsection


