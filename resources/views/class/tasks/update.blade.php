<div class="modal-header">
    <h5 class="modal-title">{{ $task->title }}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form method="post" action="{{ route('update_task') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group coд" >
            <label for="exampleFormControlInput1">Заголовок</label>
            <input required name="title" type="text" class="form-control" id="title" value="{{ $task->title }}" >
        </div>
        <div class="form-group col">
            <label for="exampleFormControlTextarea1">Описание</label>
            <textarea required name="text" class="form-control" id="text" rows="3">{{ $task->text }}</textarea>
            <input type="hidden" name="id" value="{{ $task->id }}">
        </div>
        <div class="form-group  col">
            <p>
                <a href="{{ asset( $task->attach ) }}">Вложенный файл</a>
            </p>
            <label for="exampleFormControlFile1">Прикрепите файл</label>
            <input  type="file" multiple name="file" class="form-control-file" id="exampleFormControlFile1">
        </div>
        <div class="form-group col">
            <button type="reset" class="btn btn-danger">Очистить</button>
            <button type="submit" class="btn btn-primary">Обновить</button>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
</div>