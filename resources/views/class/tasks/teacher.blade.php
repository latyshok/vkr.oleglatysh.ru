<div class="col-lg-10">

    <div class="card">
        <div class="card-header">{{ $discipline->name }}</div>

        <div class="card-body" id="tasks">

            <div class="row">

                @foreach($tasks as $task)
                    <div data-task_id="{{ $task->id }}" class="col-sm-12 col-lg-6 task">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">{{ $task->title }}</h5>
                                <p class="card-text">
                                    @if(strlen($task->text) < 150)
                                        {{ $task->text }}
                                    @else
                                        @short( $task->text )
                                    @endif
                                </p>
                                {{--<a href="#" class="btn btn-primary btn-sm">Просмотр</a>--}}
                                <button onclick="getTask('{{ $task->id }}')" data-toggle="modal" data-target=".view" class="btn btn-primary btn-sm">Редактировать</button>
                                <button onclick="delTask('{{ $task->id }}')" class="btn btn-danger btn-sm">Удалить</button>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-sm-12 col-lg-6  task">
                    <div class="card">
                        <div class="card-body add-task">
                            <a data-toggle="modal" data-target=".create" href="" class="plus"><span class="align-middle" >+</span></a>
                            <div class="content">
                                <h5 class="card-title">Новое задание</h5>
                                <p class="card-text">Чтобы добавить новое задание нажмите на + или кнопку ниже</p>
                                <button data-toggle="modal" data-target=".create" class="btn btn-success">Добавить задание</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

<div class="modal fade create" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Новое задание</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('class.tasks.create')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade create" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>


<div class="modal fade view" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>

<script>
    function delTask(id){
        $.confirm({
            title: 'Удаление!',
            content: 'Вы действительно хотите удалить задание ?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                yes: {
                    text: 'Да',
                    btnClass: 'btn-red',
                    action: function(){
                        axios.post("{{ route('del_task') }}", {
                            id : id,
                        })
                            .then(function (r) {
                                if(r.data.status){
                                    $('[data-task_id="' + id + '"]').remove();
                                }
                            })
                            .catch(function (e) {
                                console.log('DelTask: ',e);
                            });
                    }
                },
                close: {
                    text: 'Нет',
                    btnClass: 'btn-red'
                }
            }
        });
    }

    function getTask(id){
        axios.post("{{ route('get_task') }}", {
            id : id,
        })
            .then(function (r) {
                $('.view .modal-content').html(r.data);
            })
            .catch(function (e) {
                console.log('DelTask: ',e);
            });
    }

    document.addEventListener("DOMContentLoaded", function () {
        // на доработке
        // $('#text').summernote();
    });
</script>