 <form method="post" action="{{ route('create_task') }}" enctype="multipart/form-data">
     @csrf
                <div class="form-group col" >
                    <label for="exampleFormControlInput1">Заголовок</label>
                    <input required name="title" type="text" class="form-control" id="title" >
                </div>
                 <div class="form-group col">
                     <label for="exampleFormControlTextarea1">Описание</label>
                     <textarea required name="text" class="form-control" id="text" rows="3"></textarea>
                     <input type="hidden" name="user_discipline_id" value="{{ $user_discipline_id }}">
                 </div>
                <div class="form-group  col">
                     <label for="exampleFormControlFile1">Прикрепите файл</label>
                     <input type="file" multiple name="file" class="form-control-file" id="exampleFormControlFile1">
                </div>
                <div class="form-group col">
                    <button type="reset" class="btn btn-danger">Очистить</button>
                    <button type="submit" class="btn btn-primary">Создать</button>
                </div>
 </form>
