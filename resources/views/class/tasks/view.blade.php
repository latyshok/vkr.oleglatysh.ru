<div class="modal-header">
    <h5 class="modal-title">{{ $task['info']->title }}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="col">
        <h2>{{ $task['info']->title }}</h2>
        <p>{{ $task['info']->text }}</p>
        <p>
            <a href="{{ asset( $task['info']->attach ) }}">Приложение к заданию</a>
        </p>
    </div>
    @if($task['answer'])
        <div class="col">
            <p>Ваш ответ: <a href="{{ asset($task['answer']->attach) }}">Скачать</a></p>
        </div>
        @if($task['answer']->mark)
            <div class="alert alert-success" role="alert">
                Ваша оценка: {{ $task['answer']->mark }}/5
            </div>
            <div class="alert alert-info" role="alert">
                Комментарий преподавателя: {{ $task['answer']->comment }}
            </div>
        @endif
    @else
        <form method="post" action="{{ route('give_answer') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group  col">
                <label for="exampleFormControlFile1">Прикрепите файл</label>
                <input  type="file" multiple name="file" class="form-control-file" id="exampleFormControlFile1">
                <input  type="hidden" value="{{ $task['info']->id }}" name="task_id">
            </div>
            <div class="form-group col">
                <button type="reset" class="btn btn-danger btn-sm">Очистить</button>
                <button type="submit" class="btn btn-primary btn-sm">Отправить выполненное задание</button>
            </div>
        </form>
    @endif
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
</div>