@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" id="class_room">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @switch($user['role'])
                        @case('teacher')
                            @include('class.teacher')
                        @break
                        @case('student')
                            @include('class.student')
                        @break
                        @default
                            <p>Вы не являетесь преподавателем или студентом, функционал данной страницы вам недоступен</p>
                        @break
                    @endswitch
        </div>
    </div>
</div>
@endsection
