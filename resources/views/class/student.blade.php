<div class="card">
    <div class="card-header">Вы состоите в группе <b>{{ $group['name'] }}</b></div>

    <div class="card-body">
        @if( count($disciplines) )
            <div class="table-responsive">
                <table class="table">
                    <caption>Список дисциплин</caption>
                    <tbody>
                    @foreach($disciplines as $discipline)
                        <tr>
                            <td>{{ $discipline['discipline']['name'] }}</td>
                            <td>Кол-во заданий: {{ count($discipline['task']) }}</td>
                            <td>
                                <a href="{{ route('tasks', $discipline['user_discipline_id']) }}" class="btn btn-primary btn-sm">Задания</a>
                            </td>
                            <td>Средний балл: {{  $discipline['discipline']->averageMark() }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <p>Вашей группы нет в списках какой-либо дисциплины</p>
        @endif
    </div>
</div>