@extends('layouts.app')
@section('header')
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/jquery-confirm.min.js') }}" defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="{{ asset('lib/js/summernote.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('lib/css/summernote.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/jquery-confirm.min.css') }}" rel="stylesheet">
    </head>
@show
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">{{ $discipline->name }}</div>

                    <div class="card-body">
                        @if( count($answers) )
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <th>ФИО</th>
                                        <th>Группа</th>
                                        <th>Задание</th>
                                        <th>Ответ</th>
                                        <th>Оценка</th>
                                    </thead>
                                    <caption>Ответы</caption>
                                    <tbody>
                                    @foreach($answers as $answer)
                                        <tr>
                                            <td>{{ $answer['user']['name'] }}</td>
                                            <td>{{ $answer['user']->groups()->first()->name }}</td>
                                            <td>{{ $answer['task']->title }} </td>
                                            <td><a href="{{ asset( $answer['answer']->attach ) }}">Скачать</a></td>
                                            <td>
                                                @if(empty($answer['answer']->mark))
                                                        <button onclick="setMark('{{ $answer['answer']->id }}')" data-toggle="modal" data-target="#set_mark" type="button" class="btn btn-success btn-sm">Оценить</button>
                                                @else
                                                    <div class="mark">
                                                        <span>{{ $answer['answer']->mark }}/5</span>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <p>Никто еще не выполнял задания</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{--/* Модальное окно для оценки */--}}
<div id="set_mark" class="modal fade set_mark" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 data-discipline_name="" data-discipline_id="" class="modal-title">Оценка</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('set_mark') }}">
                @csrf
                <div class="modal-body">
                        <div class="form-group">
                            <label>Оценка</label>
                                <select  name="mark" class="form-control form-control-sm ">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                        </div>
                        <div class="form-group">
                            <label for="text-mail">Комментарий</label>
                            <textarea name="comment" required  id="comment" class="form-control" rows="10" placeholder="Оставьте комментарий"></textarea>
                            <input type="hidden" name="answer_id" value="">
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" >Оценить</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    function setMark(id) {
        $('[name="answer_id"]').val(id);
    }
</script>




