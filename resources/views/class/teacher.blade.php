
<div class="card">
    <div class="card-header">Учебный класс</div>

    <div class="card-body">
        @if($user['head_group'])
            <p>{{ $user['name'] }}, вы являетесь руководителем группы <b>{{ $group['name'] }}</b></p>

            <p><button type="button" class="btn btn-primary">Анализ успеваемости группы</button></p>
            <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Состав группы</button></p>
            <p><button type="button" class="btn btn-dark" data-toggle="modal" data-target="#md-send-emails">Рассылка писем</button></p>
            <p><button type="button" class="btn btn-dark" data-toggle="modal" data-target="#md-send-news">Разместить объявление</button></p>
        @else
            <p>Вы не является руководителем группы</p>
        @endif
    </div>
</div>


<div class="card">
    <div class="card-header">Управление дисциплинами</div>

    <div class="card-body">
        @if( count($user['disciplines']) )
            <div class="table-responsive">
                <table class="table">
                    <caption>Список дисциплин</caption>
                    <tbody>
                    @foreach($disciplines as $discipline)
                    <tr>
                        <td>{{ $discipline['name'] }}</td>
                        <td>
                            <a href="{{ route('tasks', $discipline->pivot->id) }}" class="btn btn-primary btn-sm">Задания</a>
                        </td>
                        <td>
                            <a  href="{{ route('answers', $discipline->pivot->id) }}"  type="button" class="btn btn-success btn-sm">Оценка</a>
                        </td>
                        <td><button onclick="getDisciplineGroup('{{ $discipline['id'] }}', '{{ $discipline->pivot->id }}')" type="button" class="btn btn-dark  btn-sm">Группы</button></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <p>Вы не преподаете ни в одной дисциплине</p>
        @endif
    </div>
</div>


{{--/* Модальное окно со списком студентов */--}}
<div id="list-student" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Состав группы</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">ФИО</th>
                            <th scope="col">E-mail</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($students as $student)
                        <tr>
                            <td>{{ $student['name'] }}</td>
                            <td>{{ $student['email'] }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


{{--/* Модальное окно со списком групп */--}}
<div id="list-groups" class="modal fade list-group" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 data-discipline_name="" data-discipline_id="" class="modal-title">Информатика</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead>
                        <tr>
                            <th colspan="2" scope="col">Группа</th>
                        </tr>
                        </thead>
                        <tbody id="group-list">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

{{--/* Модальное окно для рассылки писем */--}}
<div id="md-send-emails" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 data-discipline_name="" data-discipline_id="" class="modal-title">Отправить письмо</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="send-email" method="post" action="">
                    <div class="form-group">
                        <label for="subjectEmail">Тема письма</label>
                        <input required type="text" name="subject" class="form-control" id="subjectEmail" aria-describedby="emailHelp" placeholder="Введите тему письма">
                    </div>
                    <div class="form-group">
                        <label for="text-mail">Текст письма</label>
                        <textarea required name="text" id="text-mail" class="form-control" rows="10" placeholder="Введите текст письма"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="sendbtn" type="button" disabled class="btn btn-success" onclick="sendEmails();" data-dismiss="modal">Отправить</button>
            </div>
        </div>
    </div>
</div>

{{--/* Модальное окно для добавления новостей */--}}
<div id="md-send-news" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 data-discipline_name="" data-discipline_id="" class="modal-title">Разместить объявления</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="send-news" method="post" action="">
                    <div class="form-group">
                        <label for="subjectEmail">Заголовок</label>
                        <input required type="text" name="subject" class="form-control" id="subjectEmail" aria-describedby="emailHelp" placeholder="Введите тему письма">
                    </div>
                    <div class="form-group">
                        <label for="text-mail">Текст письма</label>
                        <textarea required name="text" id="text-mail" class="form-control" rows="10" placeholder="Введите текст письма"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input name="is_warn" type="checkbox" class="custom-control-input" id="is_warn">
                            <label class="custom-control-label" for="is_warn">Важное объявление</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="sendbtn" disabled type="button" class="btn btn-success" onclick="addNews();" data-dismiss="modal">Добавить</button>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        let inp = $('#send-email input[required], #send-email textarea[required]');
        inp.on('change keyup', function () {
            $('#md-send-emails #sendbtn').prop('disabled', validate(inp));
        });
        let inpn = $('#send-news input[required], #send-news textarea[required]').not('input[type=checkbox]');
        inpn.on('change keyup', function () {
            $('#md-send-news #sendbtn').prop('disabled', validate(inpn));
        });

    });
    function validate(elements){
        let res = false;
        $.each(elements, function (k, v) {
            if ($(v).val() === ''){
                res = true;
            }
        });
        return res;
    }
    let current_teacher = '{{ $user['id'] }}';
    function sendEmails() {
        axios.post("{{ route('send_emails') }}", {
               user_id: current_teacher,
               text: $('#send-email textarea[name=text]').val(),
               subject: $('#send-email input[name=subject]').val()
            })
            .then(function (r) {
                if (r.data.status === true)
                    toastr.success('Письмо отправлено');
                else
                    toastr.error('Что-то пошло не так');
            })
            .catch(function (err) {
                toastr.error('Что-то очень пошло не так');
            });
    }
    function addNews() {
        axios.post("{{ route('send_news') }}", {
            user_id: current_teacher,
            text: $('#send-news textarea[name=text]').val(),
            subject: $('#send-news input[name=subject]').val(),
            params: {
                warn: $('#send-news input[name=is_warn]').is(':checked')
            }
        })
            .then(function (r) {
                if (r.data.status === true)
                    toastr.success('Объявление добавлено');
                else
                    toastr.error('Что-то пошло не так');
            })
            .catch(function (err) {
                toastr.error('Что-то очень пошло не так');
            });
    }

    function getDisciplineGroup(discipline_id, user_discipline) {
        axios.post("{{ route('get_discipline_group') }}", {
            discipline_id: discipline_id,
            user_discipline: user_discipline
        })
            .then(function (r) {
                $('#group-list').html(r.data);
                $('#list-groups').modal('show');
            })
            .catch(function (err) {
                console.log(err);
            });

    }

    function delDisciplineGroup(id, discipline_id) {
        $.confirm({
            title: 'Удаление дисциплины!',
            content: 'Вы действительно хотите удалить данную группу ?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                yes: {
                    text: 'Да',
                    btnClass: 'btn-red',
                    action: function(){
                        axios.post("{{ route('del_discipline_group') }}", {
                            id: id
                        })
                            .then(function (r) {
                                // $('[data-discipline_class="' + id + '"]').remove();
                                getDisciplineGroup(discipline_id, r.data.user_discipline);
                            })
                            .catch(function (err) {
                                console.log(err);
                            });
                    }
                },
                close: {
                    text: 'Нет',
                    btnClass: 'btn-red'
                }
            }
        });
    }

    function addDisciplineGroup(user_id, discipline_id) {
        var group_id = $('#selectGroup').val();
        axios.post("{{ route('add_discipline_group') }}", {
            user_id: user_id,
            discipline_id: discipline_id,
            group_id: group_id
        })
            .then(function (r) {
                getDisciplineGroup(discipline_id, r.data.user_discipline);
            })
            .catch(function (err) {
                console.log(err);
            });

    }
</script>
