@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @switch($user->role)
                @case('teacher')
                    <div class="card">
                        <div class="card-header">Основная информация</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <p>Здравствуйте, {{Auth::user()->name}}!</p>
                            <p>Добро пожаловать в СКАУ 1.0 (Система контроля и анализа учащихся)</p>
                            <p>На этой странице можно найти справочную информацию по проекту, следуя ссылкам ниже:</p>
                            <ul>
                                <li><a href="#">Как разместить объявление</a></li>
                                <li><a href="#">Как добавить задание</a></li>
                                <li><a href="#">Как сделать рассылку студентам своей группы</a></li>
                                <li><a href="#">Как поставить оценку</a></li>
                                <li><a href="#">Как узнать состав группы, который вы руководите</a></li>
                            </ul>

                        </div>
                    </div>
                @break
                @case('student')
                    <div class="card">
                        <div class="card-header">Основная информация</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <p>Здравствуйте, {{Auth::user()->name}}!</p>
                            <p>Добро пожаловать в СКАУ 1.0 (Система контроля и анализа учащихся)</p>
                            <p>На этой странице можно найти справочную информацию по проекту, следуя ссылкам ниже:</p>
                            <ul>
                                <li><a href="#">Как перейти в профиль</a></li>
                                <li><a href="#">Как посмотреть задания</a></li>
                                <li><a href="#">Как отправить выполненное задание</a></li>
                            </ul>

                        </div>
                    </div>
                @break
                @case('admin')
                <div class="card">
                    <div class="card-header">Основная информация</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <p>Здравствуйте, {{Auth::user()->name}}!</p>
                        <p>Добро пожаловать в СКАУ 1.0 (Система контроля и анализа учащихся)</p>
                        <p>На этой странице можно найти справочную информацию по проекту, следуя ссылкам ниже:</p>
                        <ul>
                            <li><a href="#">Как добавить нового пользователя</a></li>
                            <li><a href="#">Как добавить группу</a></li>
                            <li><a href="#">Как добавить пользователя в группу</a></li>
                            <li><a href="#">Как добавить дисциплину</a></li>
                            <li><a href="#">Как назначить преподавателя на дисциплину</a></li>
                        </ul>

                    </div>
                </div>
                @break
                @default
                <p>Вы не являетесь преподавателем или студентом, функционал данной страницы вам недоступен</p>
                @break
            @endswitch
        </div>
        <div class="card-columns mt-4 col-8">
            @if(count($news))
                @foreach($news as $new)
                    @php
                            $prm = json_decode($new->params);
                            $warn = $prm->warn ? 'text-white bg-danger' : '';
                    @endphp
                    <div class="card {{ $warn }} mb-3" style="max-width: 18rem;">
                        <div class="card-header">{{ $new->title }}</div>
                        <div class="card-body">
                            <p class="card-text">{{ $new->text }}</p>
                            <p class="card-text"><small class="{{ $warn ? 'text-white' : 'text-muted' }}">{{ $new->created_at }}</small></p>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>


    </div>
</div>
@endsection
