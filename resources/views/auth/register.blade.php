<div class="" id="message" role="alert">
</div>
<form method="POST" id="form-register">
    @csrf
    <div class="form-group">
        <label for="name">ФИО</label>
        <div>
            <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="email">Email Адрес</label>
        <div>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Тип пользователя</label>
        <select name="role" class="form-control" id="exampleFormControlSelect1">
            <option value="admin">Администратор</option>
            <option value="teacher">Преподаватель</option>
            <option value="student" selected>Студент</option>
        </select>
    </div>
    <div class="form-group">
        <label for="password">Пароль</label>
        <div>
            <input autocomplete="new-password" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="password-confirm">Повторите пароль</label>
        <div>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        </div>
    </div>
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                Создать пользователя
            </button>
        </div>
    </div>
</form>
<script>
    function register(){
        var button = $("#form-register button[type='submit']");

        var message = $('#message');

        $('#form-register').on('submit', function(e){
            e.preventDefault();

            button.html('Обработка запроса...');

            axios.post('{{route('register')}}', $('#form-register').serialize())
                .then(function (response) {
                    console.log(response);
                    message.attr('class', 'alert alert-success').html('Учетная запись успешно создана ');
                    $( '#form-register' ).each(function(){
                        this.reset();
                    });

                    registerNormalaizer(response.data.data);
                })
                .catch(function (error) {
                    console.log('test:', error.response.data.errors);
                    var errors = error.response.data.errors;
                    var message = '';
                    if(errors){
                        for (key in errors) {
                            message += errors[key] + '<br>';
                        }
                        $('#message').attr('class', 'alert alert-danger').html(message);
                    } else {
                        message.attr('class', 'alert alert-danger').html('При регистрации возникла ошибка, проверьте введенные данные')
                    }
                })
                .then(function () {
                    button.html('Создать');

                    setTimeout(function () {
                        message.removeClass().html('');
                    }, 10000);
                });
        });
    }

    function registerNormalaizer(data)
    {
        switch (data.role) {
            case 'teacher':
                $('#form-register-group select').append('<option value="' + data.id + '">' + data.name + '</option>');
                $('#form-register-group').show();
                $('#list-groupcreate #informer').hide();
        }
    }
    document.addEventListener("DOMContentLoaded", function () {
           register();
    });
</script>