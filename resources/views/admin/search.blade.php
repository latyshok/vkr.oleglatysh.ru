@foreach($users as $u)
    <tr data-id="{{ $u->id }}">
        <td data-toggle="tooltip" title="{{ $u->email }}">{{ $u->name }}</td>
        <td>
            @if(\Illuminate\Support\Facades\Auth::id() == $u->id)
                {{ $user_roles[$u->role] }}
            @else
                <div  class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <button type="button" data-toggle="modal" onclick="getUserInfo('{{ $u->id }}')" data-target="#edit_user" class="btn btn-outline-info">⚙️</button>

                    <div class="btn-group" role="group">
                        <button id="user_{{ $u->id }}" type="button" class="btn btn-outline-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ $user_roles[$u->role] }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            @foreach($user_roles as $k => $role)
                                <a class="dropdown-item" style="cursor: pointer;" onclick="setRole('{{ $u->id }}', '{{ $k }}')"
                                >{{ $role }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif

        </td>
        <td><td><button onclick="delUser('{{ $u->id }}', '{{ $u->name }}')" type="button" class="btn btn-outline-danger btn-sm">✖</button></td></td>
    </tr>
@endforeach