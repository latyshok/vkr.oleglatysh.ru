@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Профиль</div>

                    <div class="card-body">

                        <p>Здравствуйте, {{ $name }}!</p>
                        <p>Ваша роль: <b>{{ $role }}</b></p>
                        <p>Ваш E-mail: <a href="malito:{{ $email }}">{{ $email }}</a> </p>

                    </div>
                </div>
                @if($dist != false)
                    <div class="card mt-3">
                        <div class="card-header">Вы преподаёте в следующих дисциплинах</div>

                        <div class="card-body">

                            @foreach($dist as $d)
                                <p>{{ $d }}</p>
                            @endforeach

                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
