<form id="search" >
    <div class="form-row ">
        @csrf
        <div class="col-xs-12 col-md-4 mb-2">
            <label for="inputState">ФИО</label>
            <input name="name" type="text" class="form-control pull-right" placeholder="Поиск по таблице">
        </div>
        <div class="col-xs-12 col-md-3 mb-2">
            <label for="inputState">Роль</label>
            <select name="role" class="form-control">
                <option value="all" selected>Не выбрана</option>
                @foreach($user_roles as $k => $role)
                    <option value="{{ $k }}">{{ $role }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-xs-12 col-md-3 mb-2">
            <label for="inputState">Группа</label>
            <select name="group"  class="form-control">
                <option value="" selected>Не выбрана</option>
                <option value="without" >Без группы</option>
                @foreach($groups as $group)
                    <option value="{{ $group['id'] }}">{{ $group['name'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-xs-12 col-md-1 mt-2 mb-2 text-center">
                <label for="inputState"></label>
                <button type="submit" class="btn btn-primary mb-2">Поиск</button>
        </div>
    </div>
</form>

<div class="table-responsive">
    <table id="mytable" class="table table-striped">
        <thead>
        <tr>
            <th scope="col">ФИО</th>
            {{--<th scope="col">Email</th>--}}
            <th scope="col">Роль</th>
        </tr>
        </thead>
        <tbody id="search_result">
            <tr><td colspan="2">Нажмите кнопку "Поиск" чтобы увидеть результаты</td></tr>
        </tbody>
    </table>



    {{-- Модальное окно --}}
    <div class="modal fade" id="edit_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Редактирование</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="edit_user_modal" action="" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="fio">ФИО</label>
                            <input type="text" class="form-control" id="fio" name="name" placeholder="ФИО">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email адресc</label>
                            <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="selectGroup">Группа</label>
                            <select name="group" class="form-control" id="selectGroup">
                                <option value="-1" selected="selected">Не выбрана</option>
                                @foreach($groups as $group)
                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" name="user_id">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="exitForm()" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="button" onclick="saveForm()" data-dismiss="modal" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    function setRole(id, role) {

        axios.post("{{ route('set_user_role') }}", {
            id: id,
            role: role
        }, {
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        }).then(function (r) {
            var btn = document.getElementById('user_'+id);
            btn.innerText = r.data.name;
            btn.classList.remove("btn-outline-info");
            btn.classList.add('btn-success');
        }).catch(function (err) {
            console.log(err);
        });
    }

    function exitForm() {
        $("#edit_user select[name='group']").val(-1);
    }

    function saveForm() {
        axios.post("{{ route('set_user') }}", $('#edit_user_modal').serialize())
            .then(function (r) {
                console.log(r.data);
                $("#edit_user select[name='group']").val(-1);
            })
            .catch(function (err) {
                console.log(err);
            });

    }

    function getUserInfo(id) {
        axios.get("{{ route('get_by_id') }}/" + id
        ).then(function (r) {
            $('#selectGroup').parent().show();

            $("#edit_user input[name='name']").val(r.data.user.name);
            $("#edit_user input[name='email']").val(r.data.user.email);
            $("#edit_user input[name='user_id']").val(r.data.user.id);
            console.log(r.data.user.role);
            console.log(r.data.user);

            if(r.data.user.role != 'student')
                $('#selectGroup').parent().hide();
            else
                $("#edit_user select[name='group']").val(r.data.groups.id);

        }).catch(function (err) {

        });
    }

    function delUser(id, name){
        $.confirm({
            title: 'Внимание!',
            content: 'Вы уверены что хотите удалить учетную запись "' + name + '"',
            buttons: {
                confirm: {
                    text: 'Да',
                    action: function(){
                        axios.post("{{ route('del_user') }}", { 'id' : id })
                            .then(function (r) {
                                if(r.data.result)
                                    $('[data-id="' + id + '"]').remove();
                                else
                                    console.error('Ошибка при удалении');
                            })
                            .catch(function (err) {
                                console.log(err);
                            });
                    }
                },
                cancel: {
                    text: 'Нет'
                }
            }
        });
    }

</script>
