<div class="table-responsive">
<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Название</th>
        <th scope="col">Руководитель</th>
        <th scope="col">Состав</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if(count($groups))
        @foreach($groups as $group)
        <tr data-group_id="{{$group['id']}}">
            <td>{{$group['name']}}</td>
            <td>{{$group['headGroup']['name']}}</td>
            <td>{{count($group['students'])}}</td>
            <td><button onclick="delGroup('{{$group['id']}}', '{{$group['name']}}')" type="button" class="btn btn-outline-danger btn-sm">✖</button></td>
        </tr>
        @endforeach
    @else
        <tr>
            <td colspan="2">Чтобы создать группу, перейдите на вкладку "Создать группу"</td>
        </tr>
    @endif
    </tbody>
</table>
</div>