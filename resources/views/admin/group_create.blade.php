<div id="group-message" role="alert">
</div>
<form {{ !count($teachers) ? 'style=display:none' : ''}} method="POST" id="form-register-group">
    @csrf
    <div class="form-group">
        <label for="name">Название</label>
        <div>
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Преподаватель</label>
        <select name="user_id" class="form-control" >
            @foreach($teachers as $teacher)
            <option value="{{$teacher['id']}}">{{$teacher['name']}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                Создать группу
            </button>
        </div>
    </div>
</form>

<p id="informer" {{ count($teachers) ? 'style=display:none' : ''}} class="alert alert-info" role="alert">Нет свободных преподавателей</p>

