<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Редактирование</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form id="edit_user_modal" action="" method="post">
                @csrf
                <div class="form-group">
                    <label for="fio">ФИО</label>
                    <input type="text" class="form-control" id="fio" name="name" placeholder="ФИО" value="{{ $user['name'] }}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email адресc</label>
                    <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="{{ $user['email'] }}">
                </div>
                <div class="form-group">
                    <label for="selectGroup">Группа</label>
                    <select name="group" class="form-control" id="selectGroup">
                        <option value="-1" selected="selected">Не выбрана</option>
                        @foreach($groups as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </div>
                <input type="hidden" name="user_id">
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" onclick="exitForm()" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="button" onclick="saveForm()" data-dismiss="modal" class="btn btn-primary">Сохранить</button>
        </div>
    </div>
</div>