@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm">
                <div class="card">
                    <div class="card-header">Распределение дисциплин</div>
                        <div class="card-body">
                            @include('admin.choise_list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
