@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm">
            <div class="card">
                <div class="card-header">Администрирование</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="row">
                            {{--Табы--}}
                            <div class="col-md-12 col-lg-4 mb-5">
                                <div class="list-group" id="list-tab" role="tablist">
                                    <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Создать учетку</a>
                                    <a class="list-group-item list-group-item-action" id="list-groupcreate-list" data-toggle="list" href="#list-groupcreate" role="tab" aria-controls="home">Создать группу</a>
                                    <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Список пользователей</a>
                                    <a class="list-group-item list-group-item-action" id="list-group-list" data-toggle="list" href="#list-group" role="tab" aria-controls="profile">Группы</a>
                                    <a class="list-group-item list-group-item-action" id="list-disc-list" data-toggle="list" href="#list-disc" role="tab" aria-controls="profile">Дисциплины</a>
                                </div>
                            </div>

                            {{--Вкладки--}}
                            <div class="col-md-12 col-lg-8 mb-5">
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                                        @include('auth.register')
                                    </div>

                                    <div class="tab-pane fade show" id="list-groupcreate" role="tabpanel" aria-labelledby="list-groupcreate-list">
                                        @include('admin.group_create')
                                    </div>

                                    <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                                        @include('admin.list_users')
                                    </div>

                                    <div class="tab-pane fade" id="list-group" role="tabpanel" aria-labelledby="list-group-list">
                                        @include('admin.group')
                                    </div>
                                    <div class="tab-pane fade" id="list-disc" role="tabpanel" aria-labelledby="list-disc-list">
                                        @include('admin.disciplines')
                                    </div>
                                </div>
                            </div>
                        </div>


                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getTab() {
        $('[data-toggle="list"]').click(function () {

            var data = {};
            data.tab = $(this).attr('id');
            data.value = {
                'group' : 'list-group-list',
                'create_group' : 'list-groupcreate-list',
            };


            axios.post("{{ route('get_tab')}}", data)
                .then(function (r) {
                    if ( r.data && data.tab == data.value.group ) {
                        $('[aria-labelledby="' + data.value.group + '"').html(r.data);
                    } else if( r.data && data.tab == data.value.create_group ) {
                        $('[aria-labelledby="' + data.value.create_group + '"').html(r.data);
                        registerGroup();
                    } else if(data.tab == 'list-profile-list') {
                        $('[aria-labelledby="' + data.tab + '"').html(r.data);

                        $('#search').on('submit', function(e) {
                            e.preventDefault();
                            search();
                        });
                    }
                })
                .catch(function (err) {
                    console.log(r);
                });
        });
    }

    // Users

    function search(){
        axios.post("{{ route('search') }}", $('#search').serialize())
            .then(function (r) {
                if(r.data)
                    $('#search_result').html(r.data);
                else
                    $('#search_result').html('<tr><td colspan=\'2\'>По вашему запросу ничего не найдено</td></tr>')
            })
            .catch(function (err) {
                console.log(err);
            });
    }

    // Groupe
    function registerGroup(){
        var button = $("button[type='submit']");

        var message_group = $('#group-message');

        $('#form-register-group').on('submit', function(e){
            e.preventDefault();

            button.html('Обработка запроса...');

            message_group.removeClass().html('');

            axios.post('{{route('create_group')}}', $('#form-register-group').serialize())
                .then(function (response) {
                    if( response.data.error ){
                        var message = response.data.error.name == 'Поле Имя имеет ошибочный формат.'
                            ? 'Формат имени группы должен соответствовать шаблону  "ИМЯГРУППЫ-Число"'
                            : response.data.error.name;
                        message_group.attr('class', 'alert alert-danger').html(message);
                        return;
                    }

                    message_group.attr('class', 'alert alert-success').html('Группа успешно создана ');
                    $( '#form-register-group' ).each(function(){
                        this.reset();
                    });

                    groupCreateNormalaizer(response.data);

                })
                .catch(function (error) {

                })
                .then(function () {
                    button.html('Создать');
                });
        });
    }

    function groupCreateNormalaizer(data) {
        $('option[value="' + $('select[name="user_id"]').val() + '"]').remove();

        if( !$('select[name="user_id"] option').length ){
            $('#form-register-group').hide();
            $('#list-groupcreate #informer').show();
        }

    }

    function delGroup(id, name){
        $.confirm({
            title: 'Внимание!',
            content: 'Вы уверены что хотите удалить группу "' + name + '"',
            buttons: {
                confirm: {
                    text: 'Да',
                    action: function(){
                        axios.post("{{ route('del_group') }}", { 'id' : id })
                            .then(function (r) {
                                if(r.data.result)
                                    $('[data-group_id="' + id + '"]').remove();
                                else
                                    console.error('Ошибка при удалении');
                            })
                            .catch(function (err) {
                                console.log(err);
                            });
                    }
                },
                cancel: {
                    text: 'Нет'
                }
            }
        });
    }

    // Discipline

    function createDiscipline(){
        let data = $('#create_disc_form').serialize();

        let name = $('input[name=disc_name]').val();
        axios.post("{{ route('create_disc') }}", data)
            .then(function (r) {
                if (r.data.exist){
                    toastr.info('Дисциплина ' + name + ' уже существует!');
                } else
                    toastr.success('Дисциплина ' + name + ' добавлена!');
                $('input[name=disc_name]').val('');
                getDiscipline();
            })
            .catch(function (e) {
                console.log('GetDiscipline: ',e);
                toastr.error('Что-то пошло не так!');
            });
    }

    function discipline() {
        $.contextMenu({
            selector: '.context-menu',
            items: {
                edit: {
                    name: "Назначить преподавателя",
                    icon: 'edit',
                    callback: function(key, opt){
                        let discipline_id = this.parent().attr('data-discipline_id');
                        axios.post("{{ route('list_teacher') }}", {
                            discipline_id : discipline_id
                        })
                            .then(function (r) {
                                console.log(r);
                                if(r.data.error){
                                    $.alert({
                                        title: 'Внимание!',
                                        content: r.data.error,
                                    });
                                } else {
                                    let content = '' +
                                        '<form action="" class="formName">' +
                                        '<div class="form-group">' +
                                        '<label>Выберите преподавателя</label>' +
                                        '<select class="name form-control" required >';
                                    $.each(r.data, function (k, v) {
                                        content += '<option value="' + v.id + '" >' + v.name + '</option>';
                                    });
                                    content += '</select>' +
                                        '</div>' +
                                        '</form>';
                                    $.confirm({
                                        title: 'Редактирование',
                                        content: content,
                                        theme: 'material',
                                        type: 'blue',
                                        buttons: {
                                            formSubmit: {
                                                text: 'Назначить',
                                                btnClass: 'btn-blue',
                                                action: function () {
                                                    let id = this.$content.find('.name').val();
                                                    axios.post("{{ route('save_disc') }}", {
                                                        teacher_id: id,
                                                        did: discipline_id
                                                    }).then(function (r) {
                                                        console.log(r.data);
                                                        getDiscipline();
                                                    });
                                                }
                                            },
                                            cancel: {
                                                text: 'Закрыть',
                                                btnClass: 'btn-default'
                                            }
                                        },
                                        onContentReady: function () {
                                            var jc = this;
                                            this.$content.find('form').on('submit', function (e) {
                                                e.preventDefault();
                                                jc.$$formSubmit.trigger('click');
                                            });
                                        }
                                    });
                                }

                            })
                            .catch(function (e) {

                            });




                    }
                },
                delete: {
                    name: "Удалить",
                    icon: "delete",
                    callback: function (key, opt) {
                        delDiscipline();
                    }
                }
            }
        });
    }

    function getDiscipline(){
        axios.post("{{ route('disc_get') }}")
            .then(function (r) {
                console.log(r.data);
                $('#list-discipline').html(r.data);
            })
            .catch(function (err) {
                console.log(err);
            });
    }

    function delDiscipline(id, name, count){
        $.confirm({
            title: 'Удаление дисциплины!',
            content: 'Вы действительно хотите удалить дисциплину "' + name + '" ?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                yes: {
                    text: 'Да',
                    btnClass: 'btn-red',
                    action: function(){
                        axios.post("{{ route('del_disc') }}", {
                            id : id,
                            count : count
                        })
                            .then(function (r) {
                                if(r.data.status){
                                    getDiscipline();
                                    toastr.success('Дисциплина "' + name + '" успешно удалена');
                                }
                            })
                            .catch(function (e) {
                                console.log('DelDiscipline: ',e);
                            });
                    }
                },
                close: {
                    text: 'Нет',
                    btnClass: 'btn-red'
                }
            }
        });
    }

    function delDisciplineTeacher(id, name, discipline_id, discipline_name){
        $.confirm({
            title: 'Удаление!',
            content: 'Вы действительно хотите удалить преподавателя "'
                + name + '" из дисциплины "' + discipline_name + '" ?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                yes: {
                    text: 'Да',
                    btnClass: 'btn-red',
                    action: function(){
                        axios.post("{{ route('del_dics_teacher') }}", {
                            id : id,
                        })
                            .then(function (r) {
                                if(r.data.status){
                                    getDiscipline();
                                    toastr.success('Преподаватель "' + name
                                        + '" успешно удален из дисциплины "' + discipline_name + '"');
                                }
                            })
                            .catch(function (e) {
                                console.log('DelDiscipline: ',e);
                            });
                    }
                },
                close: {
                    text: 'Нет',
                    btnClass: 'btn-red'
                }
            }
        });
    }

    // other

    document.addEventListener("DOMContentLoaded", function () {
        getTab();
        discipline();
    });
</script>
@endsection
