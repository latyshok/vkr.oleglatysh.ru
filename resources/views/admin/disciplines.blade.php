<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="list-discipline-tab" data-toggle="tab" href="#list-discipline" role="tab" aria-controls="list-discipline" aria-selected="true">Список</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="create-discipline-tab" data-toggle="tab" href="#create-discipline" role="tab" aria-controls="create-discipline" aria-selected="false">Создание</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="list-discipline" role="tabpanel" aria-labelledby="home-tab">
        @include('admin.discipline_list')
    </div>
    <div class="tab-pane fade" id="create-discipline" role="tabpanel" aria-labelledby="profile-tab">
        <form id="create_disc_form" class="p-2">
            <div class="form-group">
                <label for="disc_name">Название</label>
                <input type="text" class="form-control" id="disc_name" name="disc_name" placeholder="Название дисциплины">
            </div>

            <button type="button" onclick="createDiscipline();" class="btn btn-outline-primary">Создать</button>
        </form>

    </div>
</div>

