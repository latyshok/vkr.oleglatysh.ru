<div class="accordion" id="accordionExample">
@if( count($disciplines) )
@foreach($disciplines as $discipline)
    <div class="card" data-discipline_id="{{ $discipline['id'] }}">
        <div class="card-header context-menu" id="headingOne">
            <h5 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#dis{{ $discipline['id'] }}" aria-expanded="true" aria-controls="collapseOne">
                    @if(count($discipline['users']))
                        {{ $discipline['name'] }}
                    @else
                        {{ $discipline['name'] }} ( Преподаватель не назначен )
                    @endif
                </button>
                <button onclick="delDiscipline('{{ $discipline['id' ] }}', '{{ $discipline['name'] }}', '{{ count($discipline['users']) }}')" type="button" class="btn btn-outline-danger btn-sm float-right">✖</button>
            </h5>
        </div>

        <div id="dis{{ $discipline['id'] }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                @if(count($discipline['users']))
                <table id="disciplin_list" class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col" colspan="2">Преподаватель</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($discipline['users'] as $user)
                    <tr>
                        <td>{{ $user['user']['name' ] }}</td>
                        <td><button onclick="delDisciplineTeacher('{{ $user['id' ] }}', '{{ $user['user']['name'] }}', '{{ $discipline['id'] }}', '{{ $discipline['name'] }}')" type="button" class="btn btn-outline-danger btn-sm">✖</button></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                    <i>Нажмите правой кнопкой мыши на дисциплину, чтобы назначить преподавателя</i>
                @endif
            </div>
        </div>
    </div>
@endforeach
@else
    <p>Чтобы создать дисциплину перейдите на вкладку "Создание"</p>
@endif
</div>