<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersDiscipline extends Model
{
    public function discipline(){
        return $this->belongsTo('App\Discipline');
    }
}
