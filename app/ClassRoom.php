<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassRoom extends Model
{
    protected $fillable = ['user_discipline', 'group_id'];

    public function groups(){
        return $this->belongsTo('App\Group', 'group_id', 'id');
    }
}
