<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Group extends Model
{
    protected $fillable = [
        'name'
    ];

    protected static $response = [
        'success' => ['status' => 'ok'],
        'error' => ['error' => 'Ошибка при регистрации группы']
    ];

    public $timestamps = false;

    public function teachers(){
        return $this->belongsToMany('App\User', 'users_groups', 'group_id', 'user_id')
            ->where('role', 'teacher');
    }

    public function students(){
        return $this->belongsToMany('App\User', 'users_groups', 'group_id', 'user_id')
            ->where('role', 'student');
    }

    public function allUsers(){
        return $this->belongsToMany('App\User', 'users_groups', 'group_id', 'user_id');
    }

    public function headGroup(){
        return $this->hasOne('App\User', 'head_group', 'id');
    }

    public static function setTeacher(array $data)
    {
        self::$response['success']['data'] = $data;

        return  DB::table('users')
                ->where('id', $data['user_id'])
                ->update(['head_group' => $data['group_id']]) ? self::$response['success'] : self::$response['error'];
    }

    public function usersDisciplines(){
        return $this->hasManyThrough('App\UsersDiscipline', 'App\ClassRoom', 'group_id', 'id', 'id', 'user_discipline');
    }

}
