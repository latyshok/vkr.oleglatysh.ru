<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersGroup extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'group_id', 'discipline_id'
    ];
}
