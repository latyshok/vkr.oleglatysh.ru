<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Discipline extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function users(){
        //return $this->belongsToMany('App\User', 'users_disciplines', 'user_id', 'discipline_id');
        return $this->hasMany('App\UsersDiscipline', 'discipline_id', 'id');
    }

    public static function listSetTeacher($discipline_id){

        $discipline = self::find($discipline_id);

        $ids = [];

        foreach ($discipline->users as $user)
            $ids[] = $user->user_id;

        $teachers = User::where('role', 'teacher')->get(['id','name', 'email']);

        $sort = array_filter($teachers->jsonSerialize(), function ($v) use ($ids)
        {
            return !in_array($v['id'], $ids);
        });

        return $sort;
    }

    public function usersOne(){
        return $this->belongsToMany('App\User', 'users_disciplines', 'discipline_id', 'user_id')->first();
    }

    public static function withUsers()
    {
        $res = [];

        foreach (self::all(['id', 'name']) as $item)
        {
            $users = [];

            foreach ($item->users as $user)
                $users[] = ['user' => User::find($user->user_id), 'id' => $user->id];

            $res[] = ['id' => $item->id, 'name' => $item->name, 'users' => $users];
        }

        return $res;
    }

    public function usersDisciplines(){
        return $this->hasMany('App\UsersDiscipline', 'discipline_id', 'id');
    }

    public function tasks(){
        return $this->hasManyThrough('App\Task', 'App\UsersDiscipline', 'discipline_id', 'user_discipline_id');
    }

    public function averageMark()
    {
        $user = Auth::user();

        $tasks = $this->tasks()->get();

        $amount = 0;

        foreach ($tasks as $task)
            foreach ($task->answers()->where('user_id', $user->id)->get() as $answer)
                if($answer->mark)
                    $amount = $amount + $answer->mark;

        $divider = count($tasks) ?: 1;

        return round($amount / $divider);
    }
}
