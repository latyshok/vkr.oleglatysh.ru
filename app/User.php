<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Group;
use PhpParser\Builder;

class User extends Authenticatable
{
    use Notifiable;

    protected static $_roles = [
        'admin' => 'Администратор',
        'user' => 'Пользователь',
        'teacher' => 'Преподаватель',
        'student' => 'Студент'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'role', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function getAvilableRoles(){
        return self::$_roles;
    }

    public function hasRole($role)
    {
        return $this->role == $role;
    }

    public function isAppUser(){
        return $this->role == 'student' || $this->role == 'teacher';
    }

    public static function getByRole($role){
        return User::where('role', '=', $role)->get();
    }

    public function groups(){
        return $this->belongsToMany('App\Group', 'users_groups', 'user_id', 'group_id');
    }

    public function group(){
        return $this->hasOne('App\UsersGroup');
    }

    public function disciplines(){
        return $this->belongsToMany('App\Discipline', 'users_disciplines', 'user_id', 'discipline_id')->withPivot('id');
    }

    public function usersDisciplines(){
        return $this->hasMany('App\UsersDiscipline', 'user_id', 'id');
    }

    public function groupOfHead(){
        if ($this->head_group){
            return $this->hasOne('App\Group', 'id', 'head_group');
        }
        return false;
    }

    public function news()
    {
        return $this->hasManyThrough('App\News', 'App\UsersGroup', 'user_id', 'group_id', 'id', 'group_id');
    }

    public static function filter(Request $request)
    {
        if($request->has('group') && $request->input('group') == 'without'){
            $query = User::doesntHave('groups');
        } elseif($request->has('group') && $request->input('group'))
            $query = Group::find($request->input('group'))->allUsers();
        else
            $query = User::query();

        $where = [];

        if($request->has('name'))
            $where[] = ['name', 'LIKE', "%{$request->input('name')}%"];

        if($request->has('role') && $request->input('role') != 'all')
            $where[] = ['role', '=', $request->input('role')];

        $query->where($where);

        return $query->get();

        // return $query instanceof \Illuminate\Database\Eloquent\Builder ? $query->get(['id', 'name', 'email', 'role', 'head_group']) : $query;
    }
    
}
