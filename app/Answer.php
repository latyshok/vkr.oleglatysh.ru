<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'task_id',
        'user_id',
        'attach',
        'mark'
    ];

    public function users(){
        return $this->hasMany('App\User', 'id', 'user_id');
    }

}
