<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'user_discipline_id','title', 'text', 'attach'
    ];

    public function answer(){
        return $this->hasOne('App\Answer');
    }

    public function answers(){
        return $this->hasMany('App\Answer');
    }

}
