<?php

namespace App\Providers;

use function foo\func;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('short', function($string) {
            return "<?php $string = strip_tags($string);  $string = substr($string, 0, 100); $string = rtrim($string, \"!,.-\"); $string = substr($string, 0, strrpos($string, ' ')); echo $string.\"… \";  ?>";
        });
        //URL::forceScheme('https');
//        DB::listen(function ($query){
//           dump($query->sql);
//           //dump($query->building);
//        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
