<?php
/**
 * Created by PhpStorm.
 * User: migel
 * Date: 008 08 01 19
 * Time: 13:41
 */

namespace App\Http\Controllers;


use App\Discipline;
use App\UsersGroup;
use App\Group;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class UsersController extends Controller
{
    protected $data = [];

    public function index()
    {
        $this->getData();

        return view('admin.users', $this->data);
    }

    public function choise(){

        $this->getData();

        return view('admin.choise', $this->data);
    }

    public function getData()
    {
        $this->data['users'] = User::all(['id', 'name', 'email', 'role', 'head_group']);
        $this->data['user_roles'] = User::getAvilableRoles();
        $this->data['teachers'] = array_filter($this->data['users']->jsonSerialize(), function ($v){
            return $v['role'] == 'teacher' && $v['head_group'] == false;
        });
        $this->data['groups'] = Group::with('students')->with('headGroup')->get();
        $this->data['disciplines'] = Discipline::withUsers();
    }

    public function getProfile(Request $r){
        $user = Auth::user();

        $data = [
            'name' => $user->name,
            'role' => User::getAvilableRoles()[$user->role],
            'email' => $user->email,
            'dist' => false
        ];

        if ( $user->hasRole('teacher') )
            foreach ($user->disciplines as $item)
                $data['dist'][] = $item->name;

        return view('admin.profile', $data);
    }

    public function getById($id=false)
    {

        if ($id)
            $user = User::find($id);
        else
            $user = User::all(['id', 'name', 'email', 'role']);

        $groups = $user->groups()->first();

        return $user ? response()->json(compact('user', 'groups')) : response('', 404);
    }

    public function getTeachers()
    {
        $data = User::where('role', 'teacher')->get(['id','name', 'email']);

        return \response()->json($data);
    }

    public function getFilteredUser(Request $request)
    {
        if( $request->isMethod('post') ){

            $users = User::filter( $request );

            $user_roles = User::getAvilableRoles();

            $teachers = array_filter($users->jsonSerialize(), function ($v){
                return $v['role'] == 'teacher' && $v['head_group'] == false;
            });

            $groups = Group::with('students')->with('headGroup')->get();

            $content = view('admin.search', compact('users', 'user_roles', 'teachers', 'groups'))->render();

            return $content;
        }

        return back();
    }

    public function getTab(Request $request)
    {
        $view = false;

        $this->getData();

        if( $request->input('tab') ==  'list-group-list')
            $view = 'admin.group';

        if($request->input('tab') ==  'list-groupcreate-list')
            $view = 'admin.group_create';

        if($request->input('tab') ==  'list-profile-list')
            $view = 'admin.list_users';

        return $view ? view($view, $this->data)->render() : '';
    }

    public function setRole(Request $r)
    {

        if ($r->method() == 'POST') {
            $role = $r->post('role') ?: false;
            $id = $r->post('id') ?: false;
            $res = false;
            if ($role) {
                $user = User::find($id);
                $user->role = $r->get('role');
                $res = $user->save();
            }
            return json_encode(['newrole' => $role, 'name' => User::getAvilableRoles()[$role]]);
        }  else {
            return back();
        }
    }

    public function setUser(Request $r)
    {
        if ($r->isMethod('POST')){
            $data = $r->all();

            $user = User::find($data['user_id']);

            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->save();

            $wdata = [
                'user_id' => $data['user_id'],
                'group_id' => $data['group']
            ];

            $current = $user->groups()->first();

            if ($current && $current->id != $data['group'])
                UsersGroup::where('user_id', $data['user_id'])
                    ->where('group_id', $current->id)
                    ->delete();

            $res = $wdata['group_id'] == -1 ? true : UsersGroup::insert($wdata);

            return $res ? response()->json(['result' => true]) : response()->json(['result' => false]);
        }
        return back();
    }

    protected function createGroup(Request $r)
    {
        if ($r->method() == 'POST') {
            $data = $r->all();

            $v = Validator::make($data, [
                'name' => 'required|regex:/^[а-яА-Я]+-\d+$/'
            ]);

            if ($v->fails())
            {
                return response()->json(['error' => $v->errors()->messages()]);
            }

            return Group::create([
                'name' => $data['name']
            ]);
        } else {
            return back();
        }
    }

    public function delUser(Request $r){
        if ($r->isMethod('POST'))
            return  User::destroy($r->id) ? \response()->json(['result' => true]) : \response()->json(['result' => false]);
    }

}
