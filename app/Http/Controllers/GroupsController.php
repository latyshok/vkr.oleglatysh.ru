<?php

namespace App\Http\Controllers;

use App\Group;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GroupsController extends Controller
{
    protected function createGroup(Request $r)
    {
        if ( $r->isMethod('post') ) {
            $data = $r->all();

            $v = Validator::make($data, [
                'name' => 'required|unique:groups|regex:/^[А-Я]+-\d+$/'
            ]);

            if ($v->fails())
            {
                return response()->json(['error' => $v->errors()->messages()]);
            }

            $data['group_id'] = Group::create(['name' => $data['name']])['id'];

            return Group::setTeacher( $data );
        } else {
            return back();
        }
    }

    public function delGroup(Request $r){
        if($r->isMethod('POST') && Group::destroy($r->id))
            return User::where('head_group', $r->id)->update(['head_group' => null]) ? \response()->json(['result' => true]) : \response()->json(['result' => false]);
    }
}
