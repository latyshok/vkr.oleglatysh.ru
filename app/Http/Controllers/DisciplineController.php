<?php

namespace App\Http\Controllers;

use App\Discipline;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\UsersDiscipline;

class DisciplineController extends Controller
{


    public function create(Request $r)
    {

        $data = $r->all();

        if ($d = Discipline::where('name', '=', trim($data['disc_name']))->first()){

            return response()->json(['status' => true, 'exist' => true]);
        }
        else {
            $d = new Discipline();
            $d->name = $data['disc_name'];
            return response()->json(['status' => $d->save()]);
        }



    }

    public function getDiscipline(Request $r)
    {
        if( $r->isMethod('post') )
        {
            $data['disciplines'] = Discipline::withUsers();
            return view('admin.discipline_list', $data)->render() ?: '';
        }

    }

    public function getTeachers(Request $r){
        if( $r->isMethod('post') )
            return Discipline::listSetTeacher($r->discipline_id) ?: response()->json(['error' => 'Нет свободных преподавателей']);
    }

    public function delDiscipline(Request $r)
    {
        if( $r->isMethod('post') )
        {
           if( Discipline::destroy($r->id) && $r->count )
               $response = UsersDiscipline::where('discipline_id', $r->id)->delete()
                   ? ['status' => 'ok']
                   : ['error' => 'Ошибка при удалении'];
           else
               $response = ['status' => 'ok'];
        }
        return response()->json( $response );
    }

    public function delDisciplineTeacher(Request $r){
        if( $r->isMethod('post') )
            return UsersDiscipline::destroy($r->id)
                ? response()->json(['status' => 'ok'])
                : response()->json(['error' => 'Ошибка при удалении']);
    }

    public function saveDisc(Request $r)
    {
        return UsersDiscipline::insert(
            ['user_id' => $r->teacher_id, 'discipline_id' => $r->did]
        ) ? response()->json(['status' => 'ok'])
          : response()->json(['status' => 'error']);
    }


}
