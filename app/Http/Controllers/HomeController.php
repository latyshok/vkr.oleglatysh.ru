<?php

namespace App\Http\Controllers;

use App\Discipline;
use App\News;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    protected $roles = [
        'admin',
        'teacher',
        'student',
        'user'
    ];

    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        $news = [];

        if ($user->hasRole('admin'))
            $news = News::all();
        elseif($user->hasRole('teacher'))
            $news = News::where('group_id', $user->head_group)->get();
        else
            $news = $user->news()->get();

        return view('home', compact('news', 'user'));
    }




}
