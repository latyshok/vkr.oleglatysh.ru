<?php

namespace App\Http\Controllers;

use App\Answer;
use App\ClassRoom;
use App\Discipline;
use App\Group;
use App\News;
use App\Task;
use App\User;
use App\UsersDiscipline;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ClassRoomController extends Controller
{
    protected $data;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->getData();

        return view('class.class_room', $this->data);
    }

    public function getData()
    {
        $this->data['user'] = Auth::user();

        switch ( $this->data['user']->role ){
            case 'teacher' :
                $this->data['group'] = Group::find($this->data['user']->head_group);
                $this->data['disciplines'] = $this->data['user']->disciplines()->get();
                $this->data['students'] = $this->data['user']->head_group
                    ? Group::find( $this->data['user']->head_group )->students()->get()
                    : [];
                break;

            case 'student' :
                $this->data['group'] = $this->data['user']->groups()->first();
                $this->data['users_disciplines'] = $this->data['group']->usersDisciplines()->get();
                $this->data['disciplines'] = [];

                foreach ( $this->data['users_disciplines'] as $users_discipline )
                    $this->data['disciplines'][] = [
                        'discipline' => $users_discipline->discipline()->first(),
                        'task' => Task::where('user_discipline_id', $users_discipline->id)->get(),
                        'user_discipline_id' =>  $users_discipline->id,
                ];


                break;
        }

    }

    public function getDisciplineGroup(Request $r)
    {
        if($r->isMethod('post')){

            $user = Auth::user();

            $discipline_id = $r->discipline_id;

            $class_rooms = ClassRoom::where('user_discipline', $r->user_discipline)->get();

            $groups = $ids = $list_groups = [] ;

            foreach ($class_rooms as $class_room)
                foreach ($class_room->groups()->get() as $item){
                    $groups[] = ['name' => $item->name, 'class_room' => $class_room->id];
                    $ids[] = $item->id;
                }

            foreach (Group::all() as $group)
                if(!in_array($group->id, $ids))
                    $list_groups[] = $group;


            return view('class.group_list', compact('groups', 'user', 'discipline_id', 'list_groups'))->render();
        }

        return response()->json(['error' => 'Ошибка']);
    }

    public function addDisciplineGroup(Request $r)
    {
        if($r->isMethod('post')){

            $user_discipline = UsersDiscipline::where('user_id', $r->user_id)
                ->where('discipline_id', $r->discipline_id)
                ->first();

            $check =  ClassRoom::where('user_discipline',  $user_discipline['id'])
                            ->where('group_id',  $r->group_id)
                            ->first();

            if (!$check)
                return ClassRoom::create([
                'user_discipline' => $user_discipline['id'],
                'group_id' =>  $r->group_id
            ]) ? response()->json(['user_discipline' => $user_discipline['id']])
               : response()->json(['error' => 'Ошибка']);
        }

        return response()->json(['error' => 'Ошибка']);
    }

    public function delDisciplineGroup(Request $r)
    {
        if($r->isMethod('post')){
            $response = ClassRoom::find($r->id)->jsonSerialize();
            return ClassRoom::destroy($r->id)
                ? $response
                : response()->json(['error' => 'Ошибка']);
        }

        return response()->json(['error' => 'Ошибка']);
    }

    // Задания

    public function tasks($user_discipline_id)
    {
        $user = Auth::user();

        $discipline = UsersDiscipline::find($user_discipline_id)->discipline()->first();

        $tasks = [];

        if($user->hasRole('student'))
            foreach (Task::where('user_discipline_id', $user_discipline_id)->get() as $task)
                $tasks[] = [
                    'task' => $task,
                    'answer' => $task->answer()->first(),
                ];
        else
            $tasks = Task::where('user_discipline_id', $user_discipline_id)->get();

        return view('class/tasks/index', compact('user', 'discipline', 'tasks', 'user_discipline_id'));
    }

    public function createTask(Request $r)
    {
        if($r->isMethod('post'))
        {
            $file = $r->has('file')
                ? str_replace('public', 'storage', $r->file('file')->store('public'))
                : '';

            Task::create([
                'user_discipline_id' => $r->user_discipline_id,
                'title' => $r->title,
                'text' => $r->text,
                'attach' => $file
            ]);

            return redirect()->back();
        }

        return abort(404);
    }

    public function updateTask(Request $r)
    {
        if($r->isMethod('post'))
        {
            if($r->has('file'))
            {
                $attach = str_replace('storage', 'public', Task::find($r->id)->attach);

                $file = Storage::delete($attach)
                    ? str_replace('public', 'storage', $r->file('file')->store('public'))
                    : '';
            } else
                $file = Task::find($r->id)->attach;

            $task = Task::find($r->id);

            $task->title = $r->title;
            $task->text = $r->text;
            $task->attach = $file;

            $task->save();

            return redirect()->back();
        }

        return abort(404);
    }

    public function delTask(Request $r){
        if($r->isMethod('post'))
        {
            $attach = str_replace('storage', 'public', Task::find($r->id)->attach);

            Storage::delete($attach);

            return Task::destroy($r->id)
                ? response()->json(['status' => 'ok'])
                : response()->json(['error' => 'Ошибка']);
        }



        return abort(404);
    }

    public function getTask(Request $r){
        if($r->isMethod('post'))
        {
            $task = Task::find($r->id);

            return view('class.tasks.update', ['task' => $task])->render();
        }

        return abort(404);
    }

    public function showTask(Request $r)
    {
        if($r->isMethod('post'))
        {
            $task = Task::find($r->id);

            $task = [
                'info' => Task::find($r->id),
                'answer' => Task::find($r->id)->answer()->first(),
            ];

            return view('class.tasks.view', ['task' => $task])->render();
        }

        return abort(404);
    }

    // Ответы

    public function giveAnswer(Request $r)
    {
        if($r->isMethod('post'))
        {
            $file = $r->has('file')
                ? str_replace('public', 'storage', $r->file('file')->store('public'))
                : '';

            $user = Auth::user();

            Answer::create([
                'task_id' => $r->task_id,
                'user_id' => $user->id,
                'attach' => $file
            ]);

            return redirect()->back();
        }

        return abort(404);
    }

    public function answers($user_discipline_id)
    {
        $user = Auth::user();

        $tasks = Task::where('user_discipline_id', $user_discipline_id)->get();

        $answers = [];

        foreach ($tasks as $task)
            foreach ($task->answers()->get() as $answer)
            $answers[] = [
                'user' => $answer->users()->first(),
                'answer' => $answer,
                'task' => $task
            ];

        $discipline = UsersDiscipline::find($user_discipline_id)->discipline()->first();

        return view('class/answers/index', compact('user', 'discipline', 'answers'));
    }

    public function setMark(Request $r)
    {
        if($r->isMethod('post'))
        {

            $answer = Answer::find($r->answer_id);
            $answer->mark = $r->mark;
            $answer->comment = $r->comment;

            $answer->save();

            return redirect()->back();
        }

        return abort(404);
    }

    // Дополнительный функционал

    public function sendEmails(Request $r){
        $ret = [
            'status' => false,
            'count' => 0
        ];
        $this->getData();

        $subject = $r->subject;
        $text = $r->text;


        foreach ($this->data['students'] as $student) {
            if ($student instanceof User){
//                Mail::raw($text, function ($msg) use ($student, $subject){
//                    $msg->to($student->email)->subject($subject);
//                });
                Mail::send('emails.emailfromteacher', [
                    'text' => $text,
                    'fio' => $this->data['user']->name,
                    'grp' => $this->data['group']->name
                ],function ($msg) use ($student, $subject){
                    $msg->to($student->email, $student->name)->subject($subject);
                });
                $ret['count']++;
            }
        }
        $ret['status'] = $ret['count'] > 0;
        return \response()->json($ret);
    }

    public function sendNews(Request $r){
        $ret = [
            'status' => false,
        ];
        $this->getData();

        $news = new News();
        $news->title = $r->subject;
        $news->text = $r->text;
        $news->group_id = $this->data['group']->id;
        $news->params = json_encode($r->params);
        $ret['status'] = $news->save();
        return \response()->json($ret);
    }
}
