<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});


// Стандартные маршруты аутентификации...
Auth::routes(['register' => false]);


Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'class_room'], function (){
    Route::get('/', 'ClassRoomController@index')->name('class_room');
    Route::post('/get_discipline_group', 'ClassRoomController@getDisciplineGroup')->middleware('role:teacher')->name('get_discipline_group');
    Route::post('/del_discipline_group', 'ClassRoomController@delDisciplineGroup')->middleware('role:teacher')->name('del_discipline_group');
    Route::post('/add_discipline_group', 'ClassRoomController@addDisciplineGroup')->middleware('role:teacher')->name('add_discipline_group');

    Route::post('/send_news', 'ClassRoomController@sendNews')->middleware('role:teacher')->name('send_news');
    Route::post('/send_emails', 'ClassRoomController@sendEmails')->middleware('role:teacher')->name('send_emails');

    // Задания
    Route::get('/tasks/{user_discipline_id}', 'ClassRoomController@tasks')->where(['users_discipline_id' => '[0-9]+'])->name('tasks');
    Route::post('/create_task', 'ClassRoomController@createTask')->middleware('role:teacher')->name('create_task');
    Route::post('/del_task', 'ClassRoomController@delTask')->middleware('role:teacher')->name('del_task');
    Route::post('/get_task', 'ClassRoomController@getTask')->middleware('role:teacher')->name('get_task');
    Route::post('/update_task', 'ClassRoomController@updateTask')->middleware('role:teacher')->name('update_task');
    Route::post('/show_task', 'ClassRoomController@showTask')->middleware('role:student')->name('show_task');

    // Ответы
    Route::post('/give_answer', 'ClassRoomController@giveAnswer')->middleware('role:student')->name('give_answer');
    Route::post('/set_mark', 'ClassRoomController@setMark')->middleware('role:teacher')->name('set_mark');
    Route::get('/answers/{user_discipline_id}', 'ClassRoomController@answers')->middleware('role:teacher')->name('answers');
});


Route::group(['prefix' => 'admin'], function (){
    // Пользователи
    Route::get('/', 'UsersController@index')->middleware('role:admin')->name('management');
    Route::post('/users/search', 'UsersController@getFilteredUser')->middleware('role:admin')->name('search');
    Route::post('/users/edit', 'UsersController@setRole')->middleware('role:admin')->name('set_user_role');
    Route::post('/users/setuser', 'UsersController@setUser')->name('set_user');
    Route::post('/users/tab', 'UsersController@getTab')->middleware('role:admin')->name('get_tab');
    Route::get('/users/teachers', 'UsersController@getTeachers')->name('get_teachers');
    Route::get('/profile', 'UsersController@getProfile')->name('get_profile');
    Route::get('/user/{id?}', 'UsersController@getById')->name('get_by_id');
    Route::post('/users/del', 'UsersController@delUser')->middleware('role:admin')->name('del_user');

    // Группы
    Route::post('/create_group', 'GroupsController@createGroup')->middleware('role:admin')->name('create_group');
    Route::post('/del_group', 'GroupsController@delGroup')->middleware('role:admin')->name('del_group');

    // Дисциплины

    Route::post('/discipline_get', 'DisciplineController@getDiscipline')->middleware('role:admin')->name('disc_get');
    Route::post('/discipline_create', 'DisciplineController@create')->middleware('role:admin')->name('create_disc');
    Route::post('/discipline_set', 'DisciplineController@saveDisc')->middleware('role:admin')->name('save_disc');
    Route::post('/discipline_teacher', 'DisciplineController@getTeachers')->middleware('role:admin')->name('list_teacher');
    Route::post('/discipline_del', 'DisciplineController@delDiscipline')->middleware('role:admin')->name('del_disc');
    Route::post('/discipline_del_teacher', 'DisciplineController@delDisciplineTeacher')->middleware('role:admin')->name('del_dics_teacher');

    // Распределение дисциплин
    Route::get('/choise', 'UsersController@choise')->middleware('role:admin')->name('choise');


    // Антентификация
    Route::post('/register', 'Auth\RegisterController@register')->name('register');

});
